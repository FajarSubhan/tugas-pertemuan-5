/**
 * @Author  Fajar Subhan
 * @NPM     202043500578
 */

public class Example
{
    public static void main(String[] args)
    {
        String data = "Pemrograman Java";

        int count   = data.length();

        System.out.println("================== Program Pertama =========================");

        /**
         * Program Pertama
         */
        for(int foo = 0;foo <= count;foo++)
        {
            System.out.println(data.substring(foo,count));
        }


        System.out.println("================== Program Kedua =========================");

        /**
         * Program Kedua
         */
        int bar = 1;
        while (bar  <= count)
        {
            System.out.println(data.substring(bar - 1,bar++));
        }

        System.out.println("================== Program Ketiga =========================");

        /**
         * Program Ketiga
         */
        for(int baz = count - 1;baz >= 0;baz--)
        {
            System.out.println(data.substring(baz,16));
        }

    }
}
